 #include "Game.h"

Game::Game() {
	points = 0;
	
	int color = (rand()%15)+1;
	enemiesSpeed = 10;

	player = new CarPlayer(23,23,0,color);
	track = new RaceTrack();
	enemy = new CarYellow(getEnemiesInitX(),1,getEnemiesSpeed());
}

Game::~Game(){
	delete player;
	delete track;
	delete enemy;
	textcolor(WHITE);	
}

void Game::play(){
	
	
	track->draw();
	player->draw();
	
	drawUI();
	
	waitConfirmation();
	updatePoints();
	enemy -> draw();
	
	bool flag = true;
	int collisionResult = -1;
	
	while(flag) {
		listenControls();
		checkTrack();
		
		enemy -> update();
		player->update();
		
		collisionResult = detectCollisions();
		switch (collisionResult) {
		case 1: //el jugador choco con el enemigo
			player->draw();
			gameOver();
			flag = false;
			break;
		case 2: //el enemigo salio de la pista
			points++;
			if(points>0&&points%20==0){
				enemiesSpeed *= 1.3;
			}
			updatePoints();
			spanwEnemy();
			break;
		} 
	}
}

void Game::listenControls(){
	if (_kbhit()){
		int key = getch();

		switch(key){
		case (77):{ // derecha
			if (player->getExtendRight() < track->getRightLimit()){
				player->moveRight();
			} 
			break;
		}
		case (75):{ // izquierda
			if (player->getExtendLeft()> track->getLeftLimit()){
				player->moveLeft();
			} 
			break;
		}
		}
	}	
}

void Game::spanwEnemy(){
	int x0 = getEnemiesInitX();
	int y0 = getEnemiesInitY();
	int enemyType = rand()%10;
	delete enemy;
	
	if(enemyType%5==0){
		enemy = new Block(x0,y0,getEnemiesSpeed());	
	}else if(enemyType%3==0){
		enemy = new CarRed(x0,y0,getEnemiesSpeed());	
	} else {
		enemy = new CarYellow(x0,y0,getEnemiesSpeed());
	}
}

int Game::detectCollisions(){
	
	int b = enemy->getBottom();
	int l = enemy->getLeft();
	int r = enemy -> getRight();
	
	if(b>=track->getBottomLimit()){
		return 2;
	}
	
	if(b>player->getTop() && 
	   ((l >= player->getLeft() && l<= player->getRight()) || 
	   (r >= player->getLeft() && r<= player->getRight()))){
		return 1;
	}

	return 0;
}


void Game::checkTrack(){
	if (enemy->getExtendRight() >= track->getRightLimit()){
		enemy->moveLeft();
	} 

	if (enemy->getExtendLeft()<= track->getLeftLimit()){
		enemy->moveRight();
	} 

}

int Game::getEnemiesSpeed(){
	return enemiesSpeed;
}

int Game::getEnemiesInitY(){
	return 1;
}
int Game::getEnemiesInitX(){
	int widthTrack = track->getWidth();
	int minX = track->getLeftLimit();
	int x0 = (rand()%(widthTrack-3))+minX;
	return x0;
}

void Game::drawUI(){
	int x=track ->getRightLimit()+10;
	textcolor(BLACK );
	textbackground (LIGHTMAGENTA);
	gotoxy(x,5);
	cout<<"Terminal Racer";
	textbackground (0);
	textcolor(WHITE );
	gotoxy(x,8);
	cout<<"Usa las flechas derecha e izquierda para moverte hacia los lados.";
	gotoxy(x,10);
	cout<<"Preciona cualquier tecla para comenzar";

}

void Game::updatePoints(){
	int x=track ->getRightLimit()+10;
	textcolor(WHITE);
	gotoxy(x,13);
	cout<<"Puntos: "<<points;
}

void Game::gameOver(){
	int x=track ->getRightLimit()+10;
	textcolor(BLACK);
	textbackground (WHITE);
	gotoxy(x,17);
	cout<<"GAME OVER";
}

void Game::waitConfirmation(){
	bool flag = true;
	
	while(flag){
		if (_kbhit()){
			flag = false;
		}
	}
}
