	#include "Car.h"

Car::~Car(){
	clear();
}

Car::Car(){}

    Car::Car( int x0,int y0,int sp){
		
		x = x0;
		y = y0;
		
		speed = sp;
		height = 4;
		width =5;
		
		stepHorizontal = 1;
		stepVertical = 1;
		
		dx=stepHorizontal;
		dy=stepVertical;
		
		if (speed>0)
			intervalUpdate=CLOCKS_PER_SEC/speed; 
		lastUpdate=clock(); 
		
		setupView();
		
	}
	
	Car::Car( int x0,int y0,int speed, int baseColor): Car::Car(x0,y0,speed){
		setupColors(baseColor);
	}
	
	void Car::setupView(){
		view[0][0]=0;  
		view[0][1]=219; 
		view[0][2]=0;
		view[0][3]=219; 
		view[1][0]=0;  
		view[1][1]=61; 
		view[1][2]=47; 
		view[1][3]=61; 
		view[2][0]=30; 
		view[2][1]=61; 
		view[2][2]=0;  
		view[2][3]=61; 
		view[3][0]=0;  
		view[3][1]=61; 
		view[3][2]=92; 
		view[3][3]=61; 
		view[4][0]=0;  
		view[4][1]=219; 
		view[4][2]=0; 
		view[4][3]=219; 
	}
	
	void Car::setupColors(int baseColor){
		colors[0][0] = 8;
		colors[0][1] = 15;
		colors[0][2] = 8;
		colors[0][3] = 15;  
		colors[1][0] = baseColor;
		colors[1][1] = 8;
		colors[1][2] = baseColor;
		colors[1][3] = 8;
		colors[2][0] = baseColor;
		colors[2][1] = 8;
		colors[2][2] = baseColor;
		colors[2][3] = 8;
		colors[3][0] = baseColor;
		colors[3][1] = 8;
		colors[3][2] = baseColor;
		colors[3][3] = 8;
		colors[4][0] = 8;
		colors[4][1] = 15;
		colors[4][2] = 8;
		colors[4][3] = 15;
	}
	
	void Car::draw(){
		for (int i= 0; i<width; i++){
			for (int k= 0; k<height; k++){
				textcolor(colors[i][k]);
				gotoxy(x+i,y+k);
				cout<<(char) view[i][k];
			}
		}
	}
	

	void Car::move(int dX,int dY){
		dx=dX;
		dy=dY;
	}
	
	void Car::updatePosition(){
		x+=dx;
		y+=dy;
	}
	
	void Car::clear(){
		for (int i= 0; i<width; i++){
			for (int k= 0; k<height; k++){
				textcolor(0);	
				textbackground (0);
				gotoxy(x+i,y+k);
				cout<<" ";
			}
		}
	}

int Car::getTop(){return y;}

int Car::getBottom(){
	int b =y + height;
	return b; 
}
int Car::getRight(){
	int r =  x + width;
	return r;
}
int Car::getLeft(){return x ; }

int Car::getExtendLeft(){
	return getLeft() - stepHorizontal;
}
int Car::getExtendRight(){
	return getRight() + stepHorizontal;
}

void Car::moveLeft(){
	dx = -stepHorizontal;
}

void Car::moveRight(){
	dx = stepHorizontal;
}
