#ifndef CAR_YELLOW_H
#define CAR_YELLOW_H
#include "Car.h"

class CarYellow : public Car
{
private:
    
public:
    void update();
	
	void moveRight(){};
	void moveLeft(){};

    CarYellow():Car(){};
    CarYellow(int x0,int y0,int sp);
    ~CarYellow(){};
};

#endif
