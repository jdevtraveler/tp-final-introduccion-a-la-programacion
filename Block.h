#ifndef BLOCK_H
#define BLOCK_H
#include "CarYellow.h"

class Block : public CarYellow {
public:
	Block();
	Block( int x0,int y0,int sp);
protected:
	void setupView ( );
	void setupColors (int baseColor);
private:
};

#endif

