#ifndef CAR_H
#define CAR_H

#include<iostream>
#include <ctime>
#include <cstdlib>
#include <conio2.h>
#include <cstdio>
using namespace std; 

class Car {
	
public:
	virtual void update()=0;
	void draw();
	void clear(); 

	int getX(){return x;}
	int getY(){return y;}
	
	int getWidth(){return width;}
	int getHeight(){return height;}
	
	int getTop();
	int getBottom();
	int getRight();
	int getLeft();
	
	int getExtendLeft(); //get left position as it had moved to left
	int getExtendRight(); //get right position as it had moved to right
	
	virtual void moveRight();
	virtual void moveLeft();
	
	Car();
	Car( int x0,int y0,int speed);
	Car( int x0,int y0,int speed, int baseColor);
	virtual ~Car();
	
protected:
    //position
	int x;
	int y;
	
	//displacement
	int dx=0;
	int dy=0;
	
    //matrix for drawing
	int view[5][4]; 	//chars matrix
	int colors[5][4]; 	//colors matrix
    //size
	int height=4;
	int width=5;
    
    //speed
    int speed;
	
	int stepHorizontal;
	int stepVertical;
	
	virtual void setupView();
	virtual void setupColors(int baseColor);
	
	void move(int dX,int dY);
	void updatePosition();
	
	clock_t lastUpdate; //tiempo de la ultima actualización 
	clock_t intervalUpdate; //periodo entre minimo entre los actualizaciones

};

#endif
