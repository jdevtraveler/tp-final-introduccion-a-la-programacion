#ifndef CAR_RED_H
#define CAR_RED_H
#include "Car.h"

class CarRed : public Car
{
private:
    
public:
	void update();
	
	CarRed():Car(){};
	CarRed(int x0,int y0,int sp);
	~CarRed(){};
};

#endif
