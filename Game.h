#include "RaceTrack.h"
#include "CarPlayer.h"
#include "CarYellow.h"
#include "CarRed.h"
#include "Block.h"

class Game {
public:
	Game();
	~Game();
	void play();
private:
	RaceTrack *track;
	CarPlayer *player;
	Car *enemy;
	int enemiesSpeed;
	int points;
	
//	CarYellow enemy;
	void listenControls();
	int detectCollisions();
	void spanwEnemy();
	void checkTrack();
	int getEnemiesSpeed();
	int getEnemiesInitY();
	int getEnemiesInitX();
	
	void drawUI();
	void updatePoints();
	
	void waitConfirmation();
	void gameOver();
	
};

