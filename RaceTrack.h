#ifndef RACE_TRACK_H
#define RACE_TRACK_H
#include "CarPlayer.h"

class RaceTrack {

private:
	int limitLeft;
	int limitRight;
	int limitTop;
	int limitBottom;
	int barrierColor;
	int offset;
public:
	RaceTrack();
	
	void draw();
	
	int getLeftLimit(){return limitLeft;}
	int getRightLimit(){return limitRight;}
	int getTopLimit(){return limitTop;}
	int getBottomLimit(){return limitBottom;}
	int getWidth();
};

#endif

