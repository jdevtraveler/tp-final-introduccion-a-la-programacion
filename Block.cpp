#include "Block.h"

Block::Block() {}

Block::Block( int x0,int y0,int sp) :CarYellow(  x0, y0, sp*1.5){
	int color = (rand()%15)+1;
	setupColors(color);
	setupView();
}

void Block::setupView ( ) {
	for (int i= 0; i<width; i++){
		for (int k= 0; k<height; k++){
			 view[i][k] = 176;
		}
	}
	view[0][0] = 0;
	view[0][3] = 0;
	view[4][0] = 0;
	view[4][3] = 0;
}

void Block::setupColors (int baseColor) {
	for (int i= 0; i<width; i++){
		for (int k= 0; k<height; k++){
			colors[i][k] = baseColor;
		}
	}
}

