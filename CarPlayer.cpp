#include "CarPlayer.h"

CarPlayer::CarPlayer(){}

CarPlayer::CarPlayer( int x0,int y0,int speed,int baseColor): Car(x0,y0,speed,baseColor){
	stepHorizontal = 2;
	stepVertical = 0;
	
	dx=0;
	dy=0;
}

void CarPlayer::update(){
	if( dx!=0 || dy!=0){
		clear();
		updatePosition();
		draw();
		dx=0;dy=0;
	}
}
