#include "CarYellow.h"

CarYellow::CarYellow( int x0,int y0,int sp) : Car(x0,y0,sp){
	setupColors(YELLOW);
	
	stepHorizontal = 0;
	stepVertical = 1;
	
	dx=stepHorizontal;
	dy=stepVertical;
}

void CarYellow::update(){
	if(lastUpdate+intervalUpdate<clock()){
		clear();
		updatePosition();
		draw();
		lastUpdate=clock(); 
	}
}
