#ifndef CAR_PLAYER_H
#define CAR_PLAYER_H

#include "Car.h"

class CarPlayer : public Car
{
private:
    
public:

    CarPlayer();
    CarPlayer( int x0, int y0,int speed,int baseColor);
    ~CarPlayer(){};
    
	void update();
};

#endif
