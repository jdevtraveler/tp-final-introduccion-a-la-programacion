#include "RaceTrack.h"

RaceTrack::RaceTrack() {
	limitLeft = 10;
	limitRight = 40;
	limitBottom = 30;
	limitTop = 1;
	offset = 1;
	
	barrierColor = (rand()%15)+1;
	
}

void RaceTrack::draw(){
	int l = limitLeft - offset;
	int r = limitRight + offset;
	for (int i= limitTop; i<limitBottom; i++){
		textcolor(barrierColor);
		gotoxy(l,i);
		cout<<(char) 219;
		gotoxy(l-1,i);
		cout<<(char) 219;
		gotoxy(r,i);
		cout<<(char) 219;
		gotoxy(r+1,i);
		cout<<(char) 219;
	}
}

int RaceTrack::getWidth(){
	return limitRight - limitLeft;
}
