#include "CarRed.h"

CarRed::CarRed( int x0,int y0,int sp) : Car(x0,y0,sp){
	setupColors(RED);

	stepVertical = 1;
	stepHorizontal = 1;

	dy=stepVertical;

	int r = rand()%2;
	
	if(r==1){
		dx=stepHorizontal;
	} else {
		dx=-stepHorizontal;
	}
}
void CarRed::update(){
	if(lastUpdate+intervalUpdate<clock()){
		clear();
		updatePosition();
		draw();
		lastUpdate=clock(); 
	}
}
